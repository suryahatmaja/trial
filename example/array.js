//Soal No. 1 (Range)
console.log("Soal No. 1 (Range)");
function range(startNum, finishNum) 
{
    if (startNum === undefined || finishNum === undefined)
    {
        return -1;
    }
    var arr = [];
    var step = 1;
    if (startNum < finishNum)
    {
        for (var i = startNum; i <= finishNum; i += step)
        {
            arr.push(i);
        }
    }
    else
    {
        for (var i = startNum; i >= finishNum; i -= step)
        {
            arr.push(i);
        }
    }
    return arr;
} 

console.log(range(1, 10));//[1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
console.log(range(1)); // -1
console.log(range(11,18)); // [11, 12, 13, 14, 15, 16, 17, 18]
console.log(range(54, 50)); // [54, 53, 52, 51, 50]
console.log(range()) // -1